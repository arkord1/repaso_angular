import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  postRemote: any[] = [];

  posts = [
   {
    title: 'Título 1',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'

   },
   {
    title: 'Título 2',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'

   },
   {
    title: 'Título 3',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'

   }
  ];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getPosts()
    .subscribe((posts: any[]) => {
      console.log(posts);
      this.postRemote = posts;
    });
  }

}
